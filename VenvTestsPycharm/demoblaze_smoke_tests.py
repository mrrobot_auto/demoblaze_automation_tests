import unittest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium import webdriver


class DemoblazeSmokeTests(unittest.TestCase):

    @classmethod
    def setUp(self):
        self.driver = webdriver.Chrome(r"C:\Users\antyp\Desktop\chromedriver.exe")
        self.main_page_expected_title = 'STORE'
        self.product_1_expected_text = 'Samsung galaxy s6'
        self.product_2_expected_text = 'Nokia lumia 1520'
        self.product_3_expected_text = 'Nexus 6'
        self.product_4_expected_text = 'Samsung galaxy s7'
        self.product_5_expected_text = 'Iphone 6 32gb'
        self.product_6_expected_text = 'Sony xperia z5'
        self.product_7_expected_text = 'HTC One M9'
        self.product_8_expected_text = 'Sony vaio i5'
        self.product_9_expected_text = 'Sony vaio i7'
        self.product_10_expected_text = 'Apple monitor 24'
        self.product_11_expected_text = 'MacBook air'
        self.product_12_expected_text = 'Dell i7 8gb'
        self.product_13_expected_text = '2017 Dell 15.6 Inch'
        self.product_14_expected_text = 'ASUS Full HD'
        self.product_15_expected_text = 'MacBook Pro'
        self.main_url = 'https://www.demoblaze.com/'
        self.cart_page_url = self.main_url + 'cart.html'
        self.product_1_url = self.main_url + 'prod.html?idp_=1'
        self.product_2_url = self.main_url + 'prod.html?idp_=2'
        self.product_3_url = self.main_url + 'prod.html?idp_=3'
        self.product_4_url = self.main_url + 'prod.html?idp_=4'
        self.product_5_url = self.main_url + 'prod.html?idp_=5'
        self.product_6_url = self.main_url + 'prod.html?idp_=6'
        self.product_7_url = self.main_url + 'prod.html?idp_=7'
        self.product_8_url = self.main_url + 'prod.html?idp_=8'
        self.product_9_url = self.main_url + 'prod.html?idp_=9'
        self.product_10_url = self.main_url + 'prod.html?idp_=10'
        self.product_11_url = self.main_url + 'prod.html?idp_=11'
        self.product_12_url = self.main_url + 'prod.html?idp_=12'
        self.product_13_url = self.main_url + 'prod.html?idp_=13'
        self.product_14_url = self.main_url + 'prod.html?idp_=14'
        self.product_15_url = self.main_url + 'prod.html?idp_=15'
        self.product_name_xpath = '//*[@class="name"]'

    @classmethod
    def tearDown(self):
        self.driver.quit()

    def wait_for_element(self, xpath):
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, xpath)))

    def test_main_page_title(self):
        self.driver.get(self.main_url)
        title = self.driver.title
        self.assertEqual(self.main_page_expected_title, title,
                         f'Expected title: {self.main_page_expected_title} differ from actual title {title} on page: {self.main_url}')

    def test_product_1_name(self):
        self.driver.get(self.product_1_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_1_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_1_url}')

    def test_product_2_name(self):
        self.driver.get(self.product_2_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_2_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_2_url}')

    def test_product_3_name(self):
        self.driver.get(self.product_3_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_3_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_3_url}')

    def test_product_4_name(self):
        self.driver.get(self.product_4_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_4_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_4_url}')

    def test_product_5_name(self):
        self.driver.get(self.product_5_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_5_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_5_url}')

    def test_product_6_name(self):
        self.driver.get(self.product_6_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_6_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_6_url}')

    def test_product_7_name(self):
        self.driver.get(self.product_7_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_7_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_7_url}')

    def test_product_8_name(self):
        self.driver.get(self.product_8_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_8_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_8_url}')

    def test_product_9_name(self):
        self.driver.get(self.product_9_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_9_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_9_url}')

    def test_product_10_name(self):
        self.driver.get(self.product_10_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_10_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_10_url}')

    def test_product_11_name(self):
        self.driver.get(self.product_11_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_11_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_11_url}')

    def test_product_12_name(self):
        self.driver.get(self.product_12_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_12_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_12_url}')

    def test_product_13_name(self):
        self.driver.get(self.product_13_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_13_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_13_url}')

    def test_product_14_name(self):
        self.driver.get(self.product_14_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_14_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_14_url}')

    def test_product_15_name(self):
        self.driver.get(self.product_15_url)
        self.wait_for_element(self.product_name_xpath)
        product_xpath = self.driver.find_element_by_xpath(self.product_name_xpath)
        self.assertEqual(self.product_15_expected_text, product_xpath.text,
                         f'Expected product name: {self.main_page_expected_title} differ from actual: {product_xpath.text} on page: {self.product_15_url}')
import unittest
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
import time

class FrontPageTests(unittest.TestCase):

    @classmethod
    def setUp(self):
        self.driver = webdriver.Chrome(r"C:\Users\antyp\Desktop\chromedriver.exe")
        self.main_url = 'https://www.demoblaze.com/'

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def wait_for_element(self, xpath):
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, xpath)))

    def test_slider_contain_exact_number_of_slides(self):
        expected_number_of_slides = 3
        slides_xpath = '//*[@class="carousel-inner"]/div'
        self.driver.get(self.main_url)
        slide_elements = self.driver.find_elements_by_xpath(slides_xpath)
        actual_number_of_slider = len(slide_elements)
        self.assertEqual(expected_number_of_slides, actual_number_of_slider,
                         f'Slides number differ for page {self.main_url}')

    def test_slider_minimum_size(self):
        expected_min_height = 300
        expected_min_width = 780
        slider_xpath = '//*[@class="carousel-inner"]'
        self.driver.get(self.main_url)
        slide_element = self.driver.find_element_by_xpath(slider_xpath)
        actual_slide_height = slide_element.size['height']
        actual_slide_width = slide_element.size['width']
        with self.subTest('slide height'):
            self.assertLess(expected_min_height, actual_slide_height,
                            f'Slider height found by xpath {slider_xpath} on page {self.driver.current_url} is smaller than expected {expected_min_height}px')
        with self.subTest('slide width'):
            self.assertLess(expected_min_width, actual_slide_width,
                            f'Slider width found by xpath {slider_xpath} on page {self.driver.current_url} is smaller than expected {expected_min_width}px')

    def test_number_of_featured_products(self):
        expected_number_of_elements = 9
        featured_products_xpath = '//*[@class="card h-100"]'
        self.driver.get(self.main_url)
        self.wait_for_element(featured_products_xpath)
        list_of_elements = self.driver.find_elements_by_xpath(featured_products_xpath)
        actual_number_of_elements = len(list_of_elements)
        self.assertEqual(expected_number_of_elements, actual_number_of_elements, f'Products number differ for page {self.main_url}')

    def test_featured_products_price_in_usd(self):
        expected_product_price_currency = '$'
        product_price_xpath = '//*[@class="card-block"]/h5'
        self.driver.get(self.main_url)
        self.wait_for_element(product_price_xpath)
        list_of_elements = self.driver.find_elements_by_xpath(product_price_xpath)
        for price_elements in list_of_elements:
            with self.subTest(price_elements): self.assertIn(expected_product_price_currency, price_elements.text)

    def test_contact_button(self):
        expected_title = 'New message'
        contact_button_xpath = '//*[@class="nav-link"]'
        message_title_xpath = '//*[@id="exampleModalLabel"]'
        self.driver.get(self.main_url)
        self.wait_for_element(contact_button_xpath)
        button_elements_list = self.driver.find_elements_by_xpath(contact_button_xpath)
        button_element = button_elements_list[1]
        button_element.click()
        self.wait_for_element(message_title_xpath)
        actual_title = self.driver.find_element_by_xpath(message_title_xpath)
        self.assertEqual(expected_title, actual_title.text, f'Expected message title: {expected_title} differ from actual: {actual_title} for page url: {self.main_url}')

    def test_about_us_button(self):
        expected_title = 'About us'
        about_us_button_xpath = '//*[@class="nav-link"]'
        message_title_xpath = '//*[@id="videoModalLabel"]'
        self.driver.get(self.main_url)
        self.wait_for_element(about_us_button_xpath)
        button_elements_list = self.driver.find_elements_by_xpath(about_us_button_xpath)
        button_element = button_elements_list[2]
        button_element.click()
        self.wait_for_element(message_title_xpath)
        actual_title = self.driver.find_element_by_xpath(message_title_xpath)
        self.assertEqual(expected_title, actual_title.text,
                         f'Expected message title: {expected_title} differ from actual: {actual_title} for page url: {self.main_url}')

    def test_sign_up_button(self):
        expected_title = 'Sign up'
        sign_up_button_xpath = '//*[@class="nav-link"]'
        message_title_xpath = '//*[@id="signInModalLabel"]'
        self.driver.get(self.main_url)
        self.wait_for_element(sign_up_button_xpath)
        button_elements_list = self.driver.find_elements_by_xpath(sign_up_button_xpath)
        button_element = button_elements_list[7]
        button_element.click()
        self.wait_for_element(message_title_xpath)
        actual_title = self.driver.find_element_by_xpath(message_title_xpath)
        self.assertEqual(expected_title, actual_title.text,
                         f'Expected message title: {expected_title} differ from actual: {actual_title} for page url: {self.main_url}')

    def test_log_in_button(self):
        expected_title = 'Log in'
        log_in_button_xpath = '//*[@class="nav-link"]'
        message_title_xpath = '//*[@id="logInModalLabel"]'
        self.driver.get(self.main_url)
        self.wait_for_element(log_in_button_xpath)
        button_elements_list = self.driver.find_elements_by_xpath(log_in_button_xpath)
        button_element = button_elements_list[4]
        button_element.click()
        self.wait_for_element(message_title_xpath)
        actual_title = self.driver.find_element_by_xpath(message_title_xpath)
        self.assertEqual(expected_title, actual_title.text,
                         f'Expected message title: {expected_title} differ from actual: {actual_title} for page url: {self.main_url}')

# Website automated tests in Python Selenium
### Introduction
Project was made for presentation purposes. 
Used website is demo-version created for testing. 
All tests work, only requirement in code is to change webdriver path.
Other requirements are listed below in technologies section.
There are also text version of code.

### Sources

https://www.demoblaze.com/index.html

### Technologies

- Python 3.9
- PyCharm PyCharm 2021.2
- Selenium
- ChromeDriver 92.0.4515.107
- Chrome 92.0.4515.159
- Windows 10

